#ifndef IMAGE_H
#define IMAGE_H

#include <stdlib.h>
#include <stdint.h>

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel {uint8_t b, g, r;};

struct image createNewImage(uint64_t width, uint64_t height, struct pixel* data);

#endif
