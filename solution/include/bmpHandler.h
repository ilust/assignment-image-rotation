#ifndef BMPHANDLER_H
#define BMPHANDLER_H

#include <stdio.h>
#include <stdlib.h>
#include "bmp.h"
#include "Image.h"
#include "FileHandler.h"

enum bmpReadStatus{
    BMP_READ_OK = 0,
    BMP_READ_FILE_ERROR,
    BMP_READ_INVALID_SIGNATURE,
    BMP_READ_INVALID_BITS,
    BMP_READ_INVALID_BIT_COUNT,
    BMP_READ_MEMORY_ERROR,
    BMP_READ_ERROR
};

enum bmpWriteStatus{
    BMP_WRITE_OK = 0,
    BMP_WRITE_FILE_ERROR,
    BMP_WRITE_ERROR,
    BMP_WRITE_NULL_IMAGE
};


enum bmpReadStatus bmpRead(const char fileName[], struct image *img); 
enum bmpWriteStatus bmpWrite(const char fileName[], const struct image *img); 

#endif
