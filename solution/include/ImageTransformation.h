#ifndef IMAGETRANSFORMATION_H
#define IMAGETRANSFORMATION_H

#include <stdlib.h>
#include "Image.h"

enum imageTransformationStatus{
    IMAGE_TRANSFORM_OK = 0,
    IMAGE_TRANSFORM_ERROR
};

enum imageTransformationStatus imageRotateCCW(struct image *resImage, const struct image *srcImage);

#endif
