#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "FileHandler.h"
#include "bmpHandler.h"
#include "Image.h"
#include "ImageTransformation.h"

enum{OK = 0, ERROR, INPUT_ERROR, READ_ERROR, WRITE_ERROR};
