#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include <stdio.h>


enum fileOpenStatus {
    FILE_OPEN_OK = 0,
    FILE_OPEN_ERROR
};

enum fileCloseStatus {
    FILE_CLOSE_OK = 0,
    FILE_CLOSE_ERROR
};

enum fileOpenStatus fileOpen(FILE **file, const char name[], const char mode[]);
enum fileCloseStatus fileClose(FILE **file);

#endif
