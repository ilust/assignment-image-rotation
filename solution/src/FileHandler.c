#include "FileHandler.h"
#include <stdio.h>

enum fileOpenStatus fileOpen(FILE **file, const char name[], const char mode[]){
    *file = NULL;
    *file = fopen(name, mode);

    if(*file == NULL) return FILE_OPEN_ERROR;

    return FILE_OPEN_OK;
}

enum fileCloseStatus fileClose(FILE **file){
    if(fclose(*file)) return FILE_CLOSE_ERROR;

    *file = NULL;
    return FILE_CLOSE_OK;
}
