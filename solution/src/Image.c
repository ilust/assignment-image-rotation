#include "Image.h"

struct image createNewImage(uint64_t width, uint64_t height, struct pixel* data){
return (struct image){.width = width,
                      .height = height,
                      .data = data};
}
