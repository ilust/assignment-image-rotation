#include "bmpHandler.h"
#include <stdio.h>

static size_t calcPaddingSize(size_t width){return 4-(width*sizeof(struct pixel) % 4);}
static size_t calcRowSize(size_t width){return sizeof(struct pixel)*width;}
//static size_t calcRowSizeWithPadding(size_t width){return calcRowSize(width)+calcPaddingSize(width);}

static struct bmpHeader generateHeader(const struct image *img){
    const uint32_t imgSize = 3*img->width*img->height;
    return (struct bmpHeader){
        .bfType = 0x4D42,
        .bfileSize = 54 + imgSize,
        .bfReserved = 0,
        .bOffBits = 54,
        .biSize = 40,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = imgSize,
        .biXPelsPerMeter = 1000,
        .biYPelsPerMeter = 1000,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

static enum bmpReadStatus checkHeader(const struct bmpHeader *header){

    if(header->bfType != 0x4D42) return BMP_READ_INVALID_SIGNATURE;
    if(header->biBitCount != 24) return BMP_READ_INVALID_BIT_COUNT;
    if(header->biPlanes != 1) return BMP_READ_INVALID_BITS;

    return BMP_READ_OK;
}

static enum bmpReadStatus extractData(FILE* file, struct image *img){
    struct bmpHeader header;
    if(fread(&header, sizeof(struct bmpHeader), 1, file) != 1) return BMP_READ_ERROR;
    enum bmpReadStatus status = checkHeader(&header);
    if(status != BMP_READ_OK) return status;

    
    struct image tmp = createNewImage(header.biWidth,
                                    header.biHeight,
                                    malloc(sizeof(struct pixel)*header.biWidth*header.biHeight));
    if(tmp.data == NULL && tmp.width*tmp.height != 0) return BMP_READ_MEMORY_ERROR;
    
    const size_t rowSize = calcRowSize(header.biWidth);
    const size_t paddingSize = calcPaddingSize(header.biWidth);

    enum bmpReadStatus status2 = BMP_READ_OK;
    struct pixel* const p = tmp.data;
    for(size_t y = 0; y < header.biHeight; y++){
        if(fread(p + header.biWidth*y, rowSize, 1, file) != 1) {status2 = BMP_READ_ERROR; break;}
        if(fseek(file, (long)paddingSize, SEEK_CUR)) {status2 = BMP_READ_ERROR; break;}
    }

    if(status2 != BMP_READ_OK) {
        free(tmp.data);
        return status;
    }

    *img = tmp;
    return BMP_READ_OK;
}

enum bmpReadStatus bmpRead(const char fileName[], struct image *img){
    FILE *file;
    enum fileOpenStatus openStatus = fileOpen(&file, fileName, "rb");
    if(openStatus != FILE_OPEN_OK) return BMP_READ_FILE_ERROR;

    enum bmpReadStatus status = extractData(file, img);

    fileClose(&file);

    return status;
}

static enum bmpWriteStatus bmpPackData(FILE *file, const struct image *img){

    struct bmpHeader header = generateHeader(img);

    if(fseek(file, 0, SEEK_SET)) return BMP_WRITE_ERROR;
    if(fwrite(&header, sizeof(struct bmpHeader), 1, file) != 1) return BMP_WRITE_ERROR;

    const size_t paddingSize = calcPaddingSize(img->width);
    const size_t rowSize = calcRowSize(img->width);
    uint8_t trashData[4] = {0};

    struct pixel* const p = img->data;
    for(size_t y = 0; y < img->height; y++){
        if(fwrite(p + header.biWidth*y, rowSize, 1, file) != 1) return BMP_WRITE_ERROR;
        if(fwrite(trashData, paddingSize, 1, file) != 1) return BMP_WRITE_ERROR;
    }

    return BMP_WRITE_OK;
}

enum bmpWriteStatus bmpWrite(const char fileName[], const struct image *img){

    if(img == NULL) return BMP_WRITE_NULL_IMAGE;

    FILE *file;
    enum fileOpenStatus openStatus = fileOpen(&file, fileName, "wb");
    if(openStatus != FILE_OPEN_OK){
        return BMP_WRITE_FILE_ERROR;
    }

    enum bmpWriteStatus status = bmpPackData(file, img);

    fileClose(&file);

    return status;
}
