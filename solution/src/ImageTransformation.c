#include "ImageTransformation.h"

static size_t calcPixelIndex(size_t width, size_t x, size_t y){return y*width+x;}

enum imageTransformationStatus imageRotateCCW(struct image *resImage, const struct image *srcImage){
    
    if(resImage == NULL || srcImage == NULL || srcImage->data == NULL) return IMAGE_TRANSFORM_ERROR;


    struct image tmpImage = createNewImage(srcImage->height,
                                            srcImage->width,
                                            malloc(sizeof(struct pixel)*srcImage->height*srcImage->width));

    if(tmpImage.data == NULL) return IMAGE_TRANSFORM_ERROR;

    for(size_t x = 0; x < srcImage->width; x++)
        for(size_t y = 0; y < srcImage->height; y++)
            tmpImage.data[calcPixelIndex(tmpImage.width, tmpImage.width-1-y, x)] = srcImage->data[calcPixelIndex(srcImage->width, x, y)];

    *resImage = tmpImage;
    return IMAGE_TRANSFORM_OK;
}
