#include "main.h"


int main( int argc, char *argv[]) {

    //check number of provided arguments
    if(argc != 3){
        fprintf(stderr, "ERROR: Passed %d arguments. Expected 2 arguments.\n", argc-1);
        return INPUT_ERROR;
    }

    //Read image from BMP file
    struct image srcImage;
    if(bmpRead(argv[1], &srcImage) != BMP_READ_OK){
        fprintf(stderr, "ERROR: Error occured while reading image from %s\n", argv[1]);
        return READ_ERROR;
    }

    //Rotate image CCW
    struct image resImage;
    if(imageRotateCCW(&resImage, &srcImage) != IMAGE_TRANSFORM_OK){
        fprintf(stderr, "ERROR: Error occured while transforming image \n");
        return ERROR;
    }

    //Write image to BMP file
    if(bmpWrite(argv[2], &resImage) != BMP_WRITE_OK){
        fprintf(stderr, "ERROR: Error occured while writing image to %s\n",argv[2]);
        return WRITE_ERROR;
    }

    fprintf(stdout, "Done!\n");

    free(srcImage.data);
    free(resImage.data);

    return OK;
}
